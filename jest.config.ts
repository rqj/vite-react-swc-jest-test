module.exports = {
    testEnvironment: 'jsdom',
    moduleFileExtensions: ['js', 'jsx', 'ts', 'tsx', 'json', 'node'],
    transform: {
      '^.+\\.(ts|tsx|js|jsx)?$': ['@swc/jest', {
        "jsc": {
          "parser": {
            "syntax": "typescript",
            "tsx": true,
            "dynamicImport": false,
            "decorators": false
          },
          "transform": {
            "react": {
              "runtime": "automatic",
            }
          },
          "experimental": {
            "plugins": [
              ["swc-plugin-import-meta-env", {}]
            ]
          }
        }
      }]
    },
    moduleNameMapper: {
      // Handle module aliases (if you have them in your Vite config)
      '\\.css$': '<rootDir>/mockCSSModule.js',
      '\\.svg$': '<rootDir>/mockSVGModule.js',
    },
    testMatch: ['<rootDir>/src/**/?(*.)+(spec|test).+(ts|tsx|js)'],
    collectCoverageFrom: [
      'src/**/*.{js,jsx,ts,tsx}', // Adjust the path and extensions as per your project structure
      '!src/**/*.d.ts', // Exclude declaration files
    ],
    transformIgnorePatterns: [],
    //setupFiles: ['<rootDir>/jest-setup.ts'],
  };
    