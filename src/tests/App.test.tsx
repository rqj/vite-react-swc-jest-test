import { render, screen, fireEvent } from '@testing-library/react';
import App from '../App';
import '@testing-library/jest-dom';

test('renders app page', () => {
  //const { getByText: getByTextLocal } = render(<App />);
  render(<App />);
  const linkElement = screen.getByText(/Click on the Vite and React logos to learn more/i);
  expect(linkElement).toBeInTheDocument();
});

test('renders click count button', () => {
  //const { getByText: getByTextLocal } = render(<App />);
  render(<App />);
  const button = screen.getByRole('button', { name: /clickCountButton/i });
  expect(button).toBeInTheDocument();
});

test('clicks click count button', () => {
  //const { getByText: getByTextLocal } = render(<App />);
  render(<App />);
  //const button = getByTextLocal(/count is 0/i);
  const button = screen.getByRole('button', { name: /clickCountButton/i });
  fireEvent.click(button);
  expect(button).toHaveTextContent(/count is 1/i);
});
